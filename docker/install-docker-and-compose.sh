#!/bin/sh

PROJECT_URI="https://bitbucket.org/gabrielrvm/mechanical-hound"

# Installs specific Docker version
export VERSION=19.03.9

curl -sL "$PROJECT_URI/raw/master/docker/generated/get.docker.com.sh" -o - | sh 

systemctl --now enable docker

curl -sL "$PROJECT_URI/raw/master/docker/generated/run.sh" -o /usr/local/bin/docker-compose

chmod +x /usr/local/bin/docker-compose
