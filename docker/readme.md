Docker
======

Scripts para instalação do Docker e Docker Compose

## Como usar

Script de instalação: `install-docker-and-compose.sh`.

Exemplo de instalação automática:

```
curl -sL https://bitbucket.org/gabrielrvm/mechanical-hound/raw/master/docker/install-docker-and-compose.sh -o - | sh 
```

## Atualizando o instalador

Script de atualização: `update-installers.sh`
