#!/bin/sh

. '../helper.sh'

download_and_check_file \
    'https://get.docker.com/' \
    './generated/get.docker.com.sh' \
    'd174cadbbac27e161defac03f7cbe88d63f2b1f06b6a5a931bc47fa4e073f922'

download_and_check_file \
    'https://github.com/docker/compose/releases/download/1.25.5/run.sh' \
    './generated/run.sh' \
    'c3708001a80fcc5a4bb81c041fa1753f1925d21541b32b83706a87493dbcc1cd'
