#!/bin/sh

# download_file URL OUTPUTFILE
download_file() {
    curl -fsSL --fail "$1" -o "$2"
}

# check_file FILENAME HASH
check_file() {
    file="$1"
    expected="$2"
    
    calculated=$(sha256sum $file | awk '{print $1}')

    if [ "$expected" != "$calculated" ] ; then
        echo "Checksums differ: $file"
        echo "Expected  : $expected"
        echo "Calculated: $calculated"
        echo "Maybe the script has changed, verify the code and commit the changes."
        echo ""
    fi
}

# download_and_check_file URL OUTPUTFILE HASH
download_and_check_file() {
    download_and_check_file "$1" "$2"
    check_file "$2" "$3"
}
