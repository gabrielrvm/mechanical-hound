#!/bin/sh

PROJECT_URI="https://bitbucket.org/gabrielrvm/mechanical-hound"

FILE="$PROJECT_URI/raw/master/node_exporter/generated/node_exporter.tar.gz"

curl -sL $FILE | tar -xzf - --strip-components=1 --wildcards '*/node_exporter'

cp node_exporter /usr/local/bin/node_exporter

cat << EOT >> /etc/systemd/system/node_exporter.service
[Unit]
Description=Node Exporter
After=network.target
 
[Service]
Type=simple
ExecStart=/usr/local/bin/node_exporter
 
[Install]
WantedBy=multi-user.target
EOT

firewall-cmd --zone=public --add-port=9100/tcp --permanent
firewall-cmd --reload

systemctl daemon-reload
systemctl --now enable node_exporter

echo "Node exporter installed"
