Docker
======

Scripts para instalação do `node_exporter` em CentOS 7 com `unit file` para `systemd`.

## Como usar

Script de instalação: `install-node_exporter-centos7.sh`.

Exemplo de instalação automática:

```
curl -sL https://bitbucket.org/gabrielrvm/mechanical-hound/raw/master/node_exporter/install-node_exporter-centos7.sh -o - | sh 
```

### Acessando

Os dados do `node_exporter` estão disponíveis na porta padrão `9100`.

## Atualizando o instalador

Script de atualização: `update-node_exporter.sh`    
