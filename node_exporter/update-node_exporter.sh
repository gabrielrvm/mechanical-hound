#!/bin/sh

# Import helper
. '../helper.sh'

download_and_check_file \
    'https://github.com/prometheus/node_exporter/releases/download/v0.18.1/node_exporter-0.18.1.linux-amd64.tar.gz' \
    'generated/node_exporter.tar.gz' \
    'b2503fd932f85f4e5baf161268854bf5d22001869b84f00fd2d1f57b51b72424'
