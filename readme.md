The Mechanical Hound
====================

Scripts para instalação totalmente automatizada

## Motivação

Os scripts contidos neste projeto são utilizados para realizar a instalação e configuração de 
sistemas ou servidores.

A fim de minimizar os riscos, toda a cadeia de execução deve estar presente nesse projeto.
Ao invés de executar scripts diretamente de servidores de terceiros, os scripts devem ser 
analisados, validados, salvos nesse projeto, versionados, preferencialmente com checksum.

Adicionalmente, os scripts padronizam a forma e versão de instalação.

### Pre-requisitos

Cada script de instalação pode ter pre-requisitos, confira na documentação específica.

### Instalação

Os scripts de instalação são feitos para serem baixados e executados individualmente.

Por exemplo, para instalar o Docker com Docker Compose deve-se rodar o script 
`install-docker-and-compose.sh`.

Para instalações automáticas e sem intervenção humana o script pode ser baixado pela internet e 
executado:

```
curl -sL https://bitbucket.org/gabrielrvm/mechanical-hound/raw/master/docker/install-docker-and-compose.sh -o - | sh 
```

Confira na documentação específica se existem configurações ou opções adicionais.

### Atualizando os instaladores

Os arquivos de terceiros devem ser salvos no diretório `generated` de cada instalador.

É recomendado que haja scripts auxiliares para atualização e verificação.

Exemplo:

> Para o docker existe o script `update-installers.sh`, que baixa a versão mais recente dos 
> instaladores. Os instaladores são salvos na pasta `generated`. Em seguida o script compara o
> `checksum` dos arquivos com o `checksum` anteriormente registrado. Caso haja divergência, 
> provavelmente o script foi modificado.
>
> Caso o arquivo tenha sido de fato modificado, é necessário revisar as alterações inseridas.
> Após a revisão e aprovação, então o novo script deve ser incorporado ao projeto, sendo necessário
> também a atualização do(s) respectivo(s) `checksum` em `update-installers.sh`.

## Licença

Esse projeto é licenciado pela MIT License.
